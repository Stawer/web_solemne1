import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DogsService } from 'src/app/services/dogs.service';
import { Food } from 'src/app/classes/food';

@Component({
  selector: 'app-food-dog',
  templateUrl: './food-dog.component.html',
  styleUrls: ['./food-dog.component.scss']
})
export class FoodDogComponent implements OnInit {
  foods: Food[];

  constructor(private dogsService: DogsService, private router: Router) { }

  ngOnInit(): void {
    this.dogsService.getFood().subscribe(data => {
      this.foods = data.map(value => new Food(value));
   });
  }

  public showCats(){
    this.router.navigate(['']);
  }

  public showDogs(){
    this.router.navigate(['app-dog']);
  }

  public showGreenDogs(){
    this.router.navigate(['app-green-dog']);
  }

  public showFoodDogs(){
    this.router.navigate(['app-food-dog']);
  }
}
