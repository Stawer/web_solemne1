import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodDogComponent } from './food-dog.component';

describe('FoodDogComponent', () => {
  let component: FoodDogComponent;
  let fixture: ComponentFixture<FoodDogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodDogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodDogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
