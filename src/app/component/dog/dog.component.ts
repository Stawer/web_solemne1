import { Component, OnInit } from '@angular/core';
import { Dog } from 'src/app/classes/dog';
import { DogsService } from 'src/app/services/dogs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.scss']
})
export class DogComponent implements OnInit {
  public dogs: Dog[];

  constructor(private dogsService: DogsService, private router: Router) { }

  ngOnInit(): void {
    this.dogsService.getDog().subscribe(data => {
      this.dogs = data;
   });
  }

  public showCats(){
    this.router.navigate(['']);
  }

  public showDogs(){
    this.router.navigate(['app-dog']);
  }

  public showGreenDogs(){
    this.router.navigate(['app-green-dog']);
  }

  public showFoodDogs(){
    this.router.navigate(['app-food-dog']);
  }
}
