import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CatsService } from 'src/app/services/cat.service';
import { Cat } from 'src/app/classes/cat';

@Component({
  selector: '',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss']
})
export class CatComponent implements OnInit {
  public cats: Cat[];

  constructor(private catsService: CatsService, private router: Router) { }

  ngOnInit(): void {
    this.catsService.getCat().subscribe(data => {
      this.cats = data;
   });
  }

  public showCats(){
    this.router.navigate(['']);
  }

  public showDogs(){
    this.router.navigate(['app-dog']);
  }

  public showGreenDogs(){
    this.router.navigate(['app-green-dog']);
  }

  public showFoodDogs(){
    this.router.navigate(['app-food-dog']);
  }
}
