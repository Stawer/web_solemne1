import { Component, OnInit } from '@angular/core';
import { Dog } from 'src/app/classes/dog';
import { DogsService } from 'src/app/services/dogs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-green-dog',
  templateUrl: './green-dog.component.html',
  styleUrls: ['./green-dog.component.scss']
})
export class GreenDogComponent implements OnInit {
  greenDogs: Dog[];

  constructor(private dogs: DogsService, private router: Router) { }

  ngOnInit(): void {
    this.dogs.getGreenDog().subscribe(data => {
      this.greenDogs = data;
   });
  }

  public showCats(){
    this.router.navigate(['']);
  }

  public showDogs(){
    this.router.navigate(['app-dog']);
  }

  public showGreenDogs(){
    this.router.navigate(['app-green-dog']);
  }

  public showFoodDogs(){
    this.router.navigate(['app-food-dog']);
  }
}
