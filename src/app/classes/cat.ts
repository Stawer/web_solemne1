export class Cat {
    public id: number;
    public name: string;
    public color: string;
    public isFurry: boolean;
    public meows: boolean;

    constructor(data: any = null) {
        this.update(data);
    }

    private update(data): void {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.color = data.color;
            this.isFurry = data.isFurry;
            this.meows = data.meows;
        }
    }
}
