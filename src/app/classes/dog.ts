import { Food } from './food';

export class Dog {
    public id: number;
    public name: string;
    public color: string;
    public raza: string;
    public isMale: boolean;
    public food: Food[];

    constructor(data: any = null) {
        this.update(data);
    }

    private update(data): void {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.color = data.color;
            this.raza = data.raza;
            this.isMale = data.isMale;
            this.food = data.food.map(value => new Food(value));
        }
    }
}

