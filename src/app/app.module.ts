import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DogComponent } from './component/dog/dog.component';
import { DogsService } from './services/dogs.service';
import { GreenDogComponent } from './component/green-dog/green-dog.component';
import { FoodDogComponent } from './component/food-dog/food-dog.component';
import { CatComponent } from './component/cat/cat.component';
import { CatsService } from './services/cat.service';

@NgModule({
  declarations: [
    AppComponent,
    DogComponent,
    GreenDogComponent,
    FoodDogComponent,
    CatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [CatsService, DogsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
