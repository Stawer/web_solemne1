import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DogComponent } from './component/dog/dog.component';
import { GreenDogComponent } from './component/green-dog/green-dog.component';
import { FoodDogComponent } from './component/food-dog/food-dog.component';
import { CatComponent } from './component/cat/cat.component';

const routes: Routes = [{
  path: '', component: CatComponent},
  {path: 'app-dog', component: DogComponent},
  {path: 'app-green-dog', component: GreenDogComponent},
  {path: 'app-food-dog', component: FoodDogComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
