import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cat } from '../classes/cat';

@Injectable({
  providedIn: 'root'
})
export class CatsService {
  
  constructor(private http: HttpClient) { }

  public getCat():Observable<Cat[]> {
    return new Observable<Cat[]>(observe => {
      this.http.get(`../../assets/cats.json`).subscribe((data: any[]) => {
        const cats = data.map(value => new Cat(value));
        observe.next(cats);
        observe.complete();
      }, error => {
        alert ('Error al pedir los Gatos');
        observe.next(error);
        observe.complete();
      });
    });
   }
}
