import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dog } from '../classes/dog';
import { Food } from '../classes/food';

@Injectable({
  providedIn: 'root'
})
export class DogsService {

  constructor(private http: HttpClient) { }

  public getDog():Observable<Dog[]> {
    return new Observable<Dog[]>(observe => {
      this.http.get(`../../assets/dogs.json`).subscribe((data: any[]) => {
        const dogs = data.map(value => new Dog(value));
        observe.next(dogs);
        observe.complete();
      }, error => {
        alert ('Error al pedir los Perros');
        observe.next(error);
        observe.complete();
      });
    });
  }

  public getGreenDog():Observable<Dog[]> {
    return new Observable<Dog[]>(observe => {
      this.http.get(`../../assets/dogs.json`).subscribe((data: any[]) => {
        const dogs = data.map(value => new Dog(value));
        const greenDog = dogs.filter(data => data.color === 'Green');
        observe.next(greenDog);
        observe.complete();
      }, error => {
        alert ('Error al pedir los Perros');
        observe.next(error);
        observe.complete();
      });
    });
  }

  public arrayFilter(data, key) {
    return [
      ...new Map (data.map(x => [key(x),x])).values()
    ]
  }

  public getFood():Observable<Food[]> {
    return new Observable<Food[]>(observe => {
      this.http.get(`../../assets/dogs.json`).subscribe((data: any[]) => {
        const dogs = data.map(value => new Dog(value));
        let array = [];
        let filter = [];
        for (let dog of dogs) {
          for (let food of dog.food) {
            array.push(food);
          }
        }
        filter = this.arrayFilter(array,it => it.id);
        observe.next(filter);
        observe.complete();
      }, error => {
        alert ('Error al pedir la Comida');
        observe.next(error);
        observe.complete();
      });
    });
  }
}
