
FROM ubuntu:18.04

COPY . .

##Level 1

RUN apt-get update
RUN apt-get install build-essential checkinstall libssl-dev -y
ENV NODE_VERSION=12.6.0 
RUN apt install -y curl 
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash 
ENV NVM_DIR=/root/.nvm 
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} 
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION} 
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION} 
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}" 
RUN node --version 
RUN npm --version
RUN /bin/bash -c "source ~/.bashrc"


##level 2

WORKDIR /sol

COPY ./ /sol/

RUN npm config set user 0 
RUN npm config set unsafe-perm true
RUN npm install -g @angular/cli@latest
RUN npm install --save-dev @angular-devkit/build-angular
RUN ng build --prod

##level 3

RUN apt-get install nginx -y
RUN rm -rf /usr/share/nginx/html/*

COPY dist/evaluacion /usr/share/nginx/html

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]

RUN service nginx restart

###Instrucciones
##Este comando compila el docker
#[sudo] docker build . -t evaluacion:latest
##Este comando levanta la pagina en el puerto 8080 (localhost:8080)
#[sudo] docker run -d -p 8080:80 evaluacion:latest